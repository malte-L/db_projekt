
create or replace procedure intern__add_vermieter_form(
    v_nutzer_nr in nutzer.nutzer_nr%type,
    v_adress_nr in adresse.adress_nr%type,
    v_name in nutzer.name%type,
    v_vorname in nutzer.vorname%type,
    v_geschlecht in nutzer.geschlecht%type,
    v_e_mail in nutzer.e_mail%type,
    v_passwort_hash in nutzer.PASSWORT_HASH%type,
    v_beschreibung in vermieter.beschreibung%type,
    v_postleitzahl in adresse.postleitzahl%type,
    v_stadt in adresse.stadt%type,
    v_strasse in adresse.strasse%type,
    v_hausnummer in adresse.hausnummer%type)
is
begin
    insert into ADRESSE (ADRESS_NR, POSTLEITZAHL, STADT, STRASSE, HAUSNUMMER)
    values (v_adress_nr, v_postleitzahl, v_stadt, v_strasse, v_hausnummer);

    insert into NUTZER (NUTZER_NR, NAME, VORNAME, GESCHLECHT, E_MAIL, PASSWORT_HASH, ADRESS_NR)
    values (v_nutzer_nr, v_name, v_vorname, v_geschlecht, v_e_mail, v_passwort_hash, v_adress_nr);

    insert into VERMIETER (NUTZER_NR, BESCHREIBUNG)
    values (v_nutzer_nr, v_beschreibung);

    commit;
end;



create or replace procedure add_vermieter_form(
    v_name in nutzer.name%type,
    v_vorname in nutzer.vorname%type,
    v_geschlecht in nutzer.geschlecht%type,
    v_e_mail in nutzer.e_mail%type,
    v_passwort_hash in nutzer.PASSWORT_HASH%type,
    v_beschreibung in vermieter.beschreibung%type,
    v_postleitzahl in adresse.postleitzahl%type,
    v_stadt in adresse.stadt%type,
    v_strasse in adresse.strasse%type,
    v_hausnummer in adresse.hausnummer%type
) as
begin
    intern__add_vermieter_form(PRODID.nextval, PRODID.nextval, v_name, v_vorname, v_geschlecht, v_e_mail, v_passwort_hash, v_beschreibung, v_postleitzahl, v_stadt, v_strasse, v_hausnummer);
end;


begin
    add_vermieter_form(
            'Duck',
            'Daisy',
            1,
            'daisy@entenhausen.quack',
            'djhajsdhjhsajdhjashd',
            'ich bin eine ente',
            04275,
            'Entenhausen',
            'entenstrasse',
            42
        );


    add_vermieter_form(
            'Duck',
            'Donald',
            0,
            'donald@entenhausen.quack',
            'djhajsdhjhsajdhjashd',
            'ich bin eine ente',
            04275,
            'Entenhausen',
            'entenstrasse',
            42
        );


    add_vermieter_form(
            'Duck',
            'Dagobert',
            0,
            'dagobert@entenhausen.quack',
            'djhajsdhjhsajdhjashd',
            'ich bin eine ente',
            04275,
            'Entenhausen',
            'Zum Geldspeicher',
            42
        );

    add_vermieter_form(
            'Knacker',
            '0',
            0,
            '0@knackhausen.ru',
            'djhajsdhjhsajdhjashd',
            '',
            04275,
            'Entenhausen',
            'entenstrasse',
            42
        );

    add_vermieter_form(
            'Knacker',
            '1',
            0,
            '1@knackhausen.ru',
            'djhajsdhjhsajdhjashd',
            '',
            04275,
            'Entenhausen',
            'entenstrasse',
            42
        );
end;





begin
  ADD_MIETER_FORM(
            'Lehmann',
            'Malte',
            1,
            'oberbonze-pipapo@l.de',
            'weickeristcool',
            '04103',
            'Leipzig',
            'Karli',
            43
        );
end;