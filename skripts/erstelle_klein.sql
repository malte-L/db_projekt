
declare
    verm_nr_0 nutzer.nutzer_nr%type;
    mieter_nr_0 nutzer.nutzer_nr%type;

    adress_nr_0 adresse.adress_nr%type;
    adress_nr_1 adresse.adress_nr%type;

    mietobj_nr_0 mietobjekt.MIETOBJEKT_NR%type;

    standort_nr_0 standort.standort_nr%type;
BEGIN

    verm_nr_0 := PRODID.nextval;

    mieter_nr_0 := PRODID.nextval;

    adress_nr_0 := PRODID.nextval;
    adress_nr_1 := PRODID.nextval;

    standort_nr_0 := PRODID.nextval;

    mietobj_nr_0 := PRODID.nextval;


    INTERN__ADD_VERMIETER_FORM(verm_nr_0, adress_nr_0,'Müller','Donald',0,'donald.mueller@ichbinbillig.de','wad238909=K"??','Frischer Vermieter der auch gerne umsonst vermietet.','04103','Leipzig','Ölstraße','69c');

    add_mietobjekt_form(20,20,0,verm_nr_0,'45454','leipzig','karli','15',40,30);


    -- MIETER
    INTERN__ADD_MIETER_FORM(mieter_nr_0,adress_nr_1,'Judith','Runge',1,'judithrunge21@mailgen.xyz','#n/IKZ>].}kIM]^/no','85051','Ingolstadt','Otto-Lindig-Weg','152');

    intern__add_mietobjekt_form(PRODID.nextval,mietobj_nr_0,20,20,0,verm_nr_0,PRODID.nextval,'04275','leipzig','karli','15b',PRODID.nextval,15,15);

    add_boolean_option(mietobj_nr_0,  'Allgemein', 'WLAN', 1);
    ADD_STRING_OPTION(mietobj_nr_0, 'Allgemein', 'Ausstattung Küche', 'Mikrowelle; Herd; Ofen; Kühlschrank;');
    ADD_INTEGER_OPTION(mietobj_nr_0, 'Allgemein', 'Anzahl Zimmer', 3);

    ADD_ZIMMER(mietobj_nr_0, 10);
    ADD_ZIMMER(mietobj_nr_0, 20);
    ADD_ZIMMER(mietobj_nr_0, 40);

    insert into RESZENSION (INHALT, TITEL, BEWERTUNG, VERFASST_AM, RESZENSION_NR, MIETOBJEKT_NR, NUTZER_NR)
    values ('Kein WLAN obwohl in der Beschreibung dies angegeben war.', 'WLAN fehlt, leider nur 2 Sterne', 2, sysdate, PRODID.nextval, mietobj_nr_0, mieter_nr_0);


    insert into MANGEL (KURZE_BESCHREIBUNG, BESCHREIBUNG, MIETOBJEKT_NR, GEMELDETVON)
    values ('Schimmel', 'Schimmel in der Küche', mietobj_nr_0, mieter_nr_0);


    -- vermiete das Mietobjekt
    insert into VERMIETUNG (VON, BIS, MIETOBJEKT_NR, NUTZER_NR, STATUS, VERMIETUNG_NR)
    values (sysdate, sysdate + 5, mietobj_nr_0, mieter_nr_0, 2, PRODID.nextval);
    insert into VERMIETUNG (VON, BIS, MIETOBJEKT_NR, NUTZER_NR, STATUS, VERMIETUNG_NR)
    values (sysdate, sysdate + 10, mietobj_nr_0, mieter_nr_0, 2, PRODID.nextval);
    insert into VERMIETUNG (VON, BIS, MIETOBJEKT_NR, NUTZER_NR, STATUS, VERMIETUNG_NR)
    values (sysdate, sysdate + 1, mietobj_nr_0, mieter_nr_0, 2, PRODID.nextval);
END;




