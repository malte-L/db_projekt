

create or replace procedure add_zimmer(
    v_mietobjekt_nr MIETOBJEKT.MIETOBJEKT_NR%type,
    v_groesse ZIMMER.GROESSE%type
) is
begin
    intern__add_zimmer(v_mietobjekt_nr, PRODID.nextval, v_groesse);
end;


create or replace procedure intern__add_zimmer(
    v_mietobjekt_nr MIETOBJEKT.MIETOBJEKT_NR%type,
    v_zimmer_nr ZIMMER.ZIMMER_NR%type,
    v_groesse ZIMMER.GROESSE%type
) is
begin
    insert into ZIMMER (ZIMMER_NR, GROESSE, MIETOBJEKT_NR)
    values (v_zimmer_nr, v_groesse, v_mietobjekt_nr);
end;