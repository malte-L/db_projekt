

create or replace procedure add_boolean_option(
    v_mietobjekt_nr in MIETOBJEKT.MIETOBJEKT_NR%type,
    v_kategorie in OPTIONEN.KATEGORIE%type,
    v_schluessel_wert in OPTIONEN.SCHLUESSELWERT%type,
    v_wert in OPTION_BOOLEAN.WERT%type
)  is
begin
    declare
         v_option_nr OPTIONEN.OPTION_NR%type := PRODID.nextval;
    begin
        insert into OPTIONEN (MIETOBJEKT_NR, OPTION_NR, KATEGORIE, SCHLUESSELWERT)
        values (v_mietobjekt_nr, v_option_nr, v_kategorie, v_schluessel_wert);

        insert into OPTION_BOOLEAN (OPTION_NR, WERT)
        values (v_option_nr, v_wert);
    end;
end;


create or replace procedure add_string_option(
    v_mietobjekt_nr in MIETOBJEKT.MIETOBJEKT_NR%type,
    v_kategorie in OPTIONEN.KATEGORIE%type,
    v_schluessel_wert in OPTIONEN.SCHLUESSELWERT%type,
    v_wert in option_string.WERT%type
)  is
begin
    declare
        v_option_nr OPTIONEN.OPTION_NR%type := PRODID.nextval;
    begin
        insert into OPTIONEN (MIETOBJEKT_NR, OPTION_NR, KATEGORIE, SCHLUESSELWERT)
        values (v_mietobjekt_nr, v_option_nr, v_kategorie, v_schluessel_wert);

        insert into option_string (OPTION_NR, WERT)
        values (v_option_nr, v_wert);
    end;
end;

create or replace procedure add_integer_option(
    v_mietobjekt_nr in MIETOBJEKT.MIETOBJEKT_NR%type,
    v_kategorie in OPTIONEN.KATEGORIE%type,
    v_schluessel_wert in OPTIONEN.SCHLUESSELWERT%type,
    v_wert in option_integer.WERT%type
)  is
begin
    declare
        v_option_nr  OPTIONEN.OPTION_NR%type := PRODID.nextval;
    begin
        insert into OPTIONEN (MIETOBJEKT_NR, OPTION_NR, KATEGORIE, SCHLUESSELWERT)
        values (v_mietobjekt_nr, v_option_nr, v_kategorie, v_schluessel_wert);

        insert into option_integer (OPTION_NR, WERT)
        values (v_option_nr, v_wert);
    end;
end;


