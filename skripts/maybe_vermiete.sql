create or replace procedure maybe_vermiete(
    v_von VERMIETUNG.VON%type,
    v_bis VERMIETUNG.bis%type,
    v_nutzer_nr VERMIETUNG.nutzer_nr%type,
    v_mietobjekt_nr VERMIETUNG.MIETOBJEKT_NR%type
) is
begin
    declare
        dummy number;
        v_vermietung_nr VERMIETUNG.VERMIETUNG_NR%type := PRODID.nextval;
    begin
        select VERMIETUNG_NR into dummy from VERMIETUNG
        where MIETOBJEKT_NR = v_mietobjekt_nr
        -- and STATUS = 2 -- schon vom vermieter bestätigte vermietung
        and ((v_von between (VON-1) and (BIS+1)) or (v_bis between (VON-1) and (BIS+1)))
        and rownum = 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN begin
        insert into VERMIETUNG (VON, BIS, MIETOBJEKT_NR, NUTZER_NR, VERMIETUNG_NR)
        values (v_von, v_bis, v_mietobjekt_nr, v_nutzer_nr, v_vermietung_nr);
        commit;
        return;
      end;
    end;

    raise_application_error(-20111, 'Mietzeitraum schon vergeben');
end;


begin
    maybe_vermiete(
        sysdate + 23,
        sysdate + 24,
        201378,
        201387
    );
end;

