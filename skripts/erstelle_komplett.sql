
declare
    verm_nr_0 nutzer.nutzer_nr%type;
    mieter_nr_0 nutzer.nutzer_nr%type;

    adress_nr_0 adresse.adress_nr%type;
    adress_nr_1 adresse.adress_nr%type;

    mietobj_nr_0 mietobjekt.MIETOBJEKT_NR%type;

    standort_nr_0 standort.standort_nr%type;
BEGIN
    verm_nr_0 := 0;

    mieter_nr_0 := 0;

    adress_nr_0 := 0;
    adress_nr_1 := 1;

    standort_nr_0 := 0;


  INTERN__ADD_VERMIETER_FORM(
    verm_nr_0,
      adress_nr_0,
    'Müller',
    'Tobi',
    0,
    'tobi.mueller@ichbinbillig.de',
    'wad238909=K"??',
    'Frischer Vermieter der auch gerne umsonst vermietet.',
    '04103',
    'Leipzig',
    'Ölstraße',
    '69c'
    );

    ADD_VERMIETER_FORM(
    'Baur',
    'Nikolai',
    0,
    'n.baur@mailgen.club',
    'W`hI}&?bux<.\C<b&l??',
    'Geile Hütten, geile Preise',
    '14163',
    'Berlin',
    'Bogotastr.',
    '27c'
      );

    ADD_VERMIETER_FORM(
    'Klemm',
    'Reiner',
    0,
    'tr.k@mailme.xxx',
    'UjpO=?mVg;9IYr6XR?',
    'Freundliche Vermietung und guter Service von Haus aus.',
    '45881',
    'Gelsenkirchen',
    'Alfred-Zingler-Str.',
    '9a'
      );

    ADD_VERMIETER_FORM(
    'Harms',
    'Maren',
    1,
    'maren@hamster.de',
    'wad23324r2f256ß21++2+',
    'Wohnungen mit Stiel und Ikone',
    '67745',
    'Grumbach',
    'Hardenbergstraße',
    '3c'
      );

    ADD_VERMIETER_FORM(
    'Michels',
    'Silvio',
    0,
    'silviomichels@mailgen.info',
    'S|C<8-!N{[t3A',
    'Web Developer',
    '52076',
    'Aachen',
    'Schleidener Str.',
    '122b'
      );

    ADD_VERMIETER_FORM(
    'Bader',
    'Hanna',
    1,
    'sandra.stahl@kremer.de',
    '<L+56f6="DAx?nS',
    'Bulldozer Operator',
    '40233',
    'Düsseldorf Oberbilk',
    'Park Str',
    '22b'
      );

    ADD_VERMIETER_FORM(
    'Mayer',
    'Marc',
    0,
    'm.m@mailgen.fun',
    'w245vd3d3890923e5t2256?',
    'Solderer',
    '22529',
    'Hamburg Stellingen',
    'Los Angeles Platz',
    '91c'
      );

    ADD_VERMIETER_FORM(
    'Stein',
    'Reinhard',
    0,
    'john.lucie@john.org',
    'wad23890923423232=K!?',
    'Frischer Software Engineer',
    '47509',
    'Rheurdt',
    'Gotzkowskystraße',
    '58'
      );


    add_mietobjekt_form(
      20,
        20,
        0,
        verm_nr_0,
        '45454',
        'leipzig',
        'karli',
        15,
        40,
        30
    );


    -- MIETER

    INTERN__ADD_MIETER_FORM(
        mieter_nr_0,
        adress_nr_1,
    'Judith',
    'Runge',
    1,
    'judithrunge21@mailgen.xyz',
    '#n/IKZ>].}kIM]^/no',
    '85051',
    'Ingolstadt',
    'Otto-Lindig-Weg',
    '152'
    );
		    ADD_MIETER_FORM(
            'Ortwin',
            'Mertens',
            0,
            'o.m@mailgen.pw',
            ']<k"8oTY&MTm?i|fqJQ',
            '44869',
            'Bochum',
            'Narzissenstr.',
            '104a'
        );
		    ADD_MIETER_FORM(
            'Marietta',
            'Pape',
            1,
            'mariettap@mailgen.club',
            'dLO\&*6.KV@Q)l!|^r',
            '99084',
            'Erfurt',
            'Vor dem Moritztor',
            '158b'
        );
				    ADD_MIETER_FORM(
            'Rosalinde',
            'Schreiner',
            1,
            'rosalinde.s@mailgen.xyz',
            'y/]ep#N+ckre9',
            '42651',
            'Solingen',
            'Flurstr.',
            '37a'
        );
		    ADD_MIETER_FORM(
            'Ismail',
            'Hoppe',
            1,
            'i.h1701@mailgen.fun',
            'D<sG$\@{@%3Vw[06_-:',
            '87553',
            'Oberstdorf',
            'Alt Reinickendorf',
            '166b'
        );
				    ADD_MIETER_FORM(
            'Gisela',
            'Marx',
            1,
            'gisela.m@mailgen.fun',
            'LV@>\pAm?V`6El',
            '71263',
            'Weil der Stadt',
            'Hallesches Ufer',
            '180'
        );
		    ADD_MIETER_FORM(
            'Dirk',
            'Jacobs',
            0,
            'dirk.jacobs@mailgen.pro',
            'AX=[`N|VX^Gy#JVDYMO',
            '91365',
            'Weilersbach',
            'Ellmenreichstrasse',
            '160'
        );
		    ADD_MIETER_FORM(
            'Georgios',
            'Singer',
            0,
            'georgiossinger21@mailgen.xyz',
            'yw`>qC2dX"(hGyo9gyHs',
            '89079',
            'Ulm',
            'Kirchgasse',
            '69a'
        );
		    ADD_MIETER_FORM(
            'Ria',
            'Wegner',
            1,
            'ria.w21@mailgen.club',
            'nwLl_Jre/M',
            '85057',
            'Ingolstadt',
            'Kroppstr.',
            '20'
        );
		    ADD_MIETER_FORM(
            'Reiner',
            'Henkler',
            0,
            'reiner.wenkler@gmail.com',
            '34@dl3//d3',
            '04103',
            'Leipzig',
            'Straße des 18. Oktober',
            '143c'
        );
				    ADD_MIETER_FORM(
            'Hans-Dieter',
            'Sauer',
            0,
            'hansdieters@mailgen.club',
            'Z<w"W;mm2U>o|u^zZgy',
            '94086',
            'Griesbach',
            'Ziegelstr',
            '25a'
        );
		ADD_MIETER_FORM(
            'Annelies',
            'Heinz',
            1,
            'a.heinz1701@mailgen.biz',
            '0ZOuN$Lj1z<+N',
            '02607',
            'Bautzen',
            'Heinrich Heine Platz',
            '191c'
        );
            ADD_MIETER_FORM(
        'Meike',
        'Jokel',
        1,
        'meike96@freenet.de',
        '0feZO345uN?!',
        '38899',
        'Blankenburg',
        'Halberstädterstraße',
        '45'
    );
            ADD_MIETER_FORM(
        'Falko',
        'Schlüter',
        0,
        'fs@mailgen.club',
        '|)a=e$&h]iM;/iB`t',
        '99100',
        'Erfurt',
        'Erfurter Tor',
        '131'
    );
            ADD_MIETER_FORM(
        'Miroslaw',
        'Unger',
        0,
        'm.u1701@web.de',
        '0ZOuN$Lj1z<+N',
        '33100',
        'Paderborn',
        'Becker Gärten',
        '126b'
    );
            ADD_MIETER_FORM(
        'Traudel',
        'Lohmann',
        1,
        'traudel.lohmann21@t-online.de',
        '?H,i)_7j2#',
        '42897',
        'Remscheid',
        'Eibenweg',
        '53a'
    );



    add_mietobjekt_form(
      20,
        20,
        0,
        verm_nr_0,
        '45454',
        'leipzig',
        'karli',
        15,
        40,
        30
    );


    select MIETOBJEKT_NR into :mietobj_nr_0
    from MIETOBJEKT where VERMIETER_NR = verm_nr_0;

    add_boolean_option(mietobj_nr_0,  'Allgemein', 'WLAN', 1);


    insert into RESZENSION (INHALT, TITEL, BEWERTUNG, VERFASST_AM, RESZENSION_NR, MIETOBJEKT_NR, NUTZER_NR)
    values ('Kein WLAN obwohl in der Beschreibung dies angegeben war.', 'WLAN fehlt, leider nur 2 Sterne', 2, sysdate, PRODID.nextval, mietobj_nr_0, verm_nr_0);


    insert into MANGEL (KURZE_BESCHREIBUNG, BESCHREIBUNG, MIETOBJEKT_NR, GEMELDETVON)
    values ('Schimmel', 'Schimmel in der Küche', mietobj_nr_0, mieter_nr_0);

END;



delete KOORDINATEN;
delete ADRESSE;
delete MANGEL;

select * from MIETER;


delete STANDORT;
delete OPTIONEN;
delete vermietung;
delete MIETOBJEKT;
delete NUTZER;

