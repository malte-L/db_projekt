




create or replace procedure intern__add_mietobjekt_form (
    v_titel in varchar2,
    v_desc in varchar2,
    v_standort_nr in standort.standort_nr%type,
    v_mietobjekt_nr in mietobjekt.mietobjekt_nr%type,
    v_mietpreis_HS in mietobjekt.mietpreis_HS%type,
    v_mietpreis_NS in mietobjekt.mietpreis_NS%type,
    v_status in mietobjekt.status%type,
    v_vermieter_nr in mietobjekt.vermieter_nr%type,
    v_ADRESS_NR in adresse.ADRESS_NR%type,
    v_POSTLEITZAHL in adresse.POSTLEITZAHL%type,
    v_STADT in adresse.STADT%type,
    v_STRASSE in adresse.STRASSE%type,
    v_HAUSNUMMER in adresse.HAUSNUMMER%type,
    v_KOORDINATEN_NR in koordinaten.KOORDINATEN_NR%type,
    v_LAENGENGRAD in koordinaten.LAENGENGRAD%type,
    v_BREITENGRAD in koordinaten.BREITENGRAD%type
)
is
begin
    insert into ADRESSE (ADRESS_NR, POSTLEITZAHL, STADT, STRASSE, HAUSNUMMER)
    values (v_ADRESS_NR, v_POSTLEITZAHL, v_STADT, v_STRASSE, v_HAUSNUMMER);

    insert into KOORDINATEN (KOORDINATEN_NR, LAENGENGRAD, BREITENGRAD)
    values (v_KOORDINATEN_NR, v_LAENGENGRAD, v_BREITENGRAD);

    insert into STANDORT (STANDORT_NR, KOORDINATEN_NR, ADRESS_NR)
    values (v_standort_nr, v_KOORDINATEN_NR, v_adress_nr);

    insert into MIETOBJEKT (MIETOBJEKT_NR, STANDORT_NR, MIETPREIS_HS, MIETPREIS_NS, STATUS, VERMIETER_NR)
    values (v_mietobjekt_nr, v_standort_nr, v_mietpreis_HS, v_mietpreis_NS, v_status, v_vermieter_nr);

    ADD_STRING_OPTION(v_mietobjekt_nr, 'meta', 'titel', v_titel);
    ADD_STRING_OPTION(v_mietobjekt_nr, 'meta', 'desc', v_desc);
    commit;
end;


create or replace procedure add_mietobjekt_form (
    v_titel in varchar2,
    v_desc in varchar2,
    v_mietpreis_HS in mietobjekt.mietpreis_HS%type,
    v_mietpreis_NS in mietobjekt.mietpreis_NS%type,
    v_status in mietobjekt.status%type,
    v_vermieter_nr in mietobjekt.vermieter_nr%type,
    v_POSTLEITZAHL in adresse.POSTLEITZAHL%type,
    v_STADT in adresse.STADT%type,
    v_STRASSE in adresse.STRASSE%type,
    v_HAUSNUMMER in adresse.HAUSNUMMER%type,
    v_LAENGENGRAD in koordinaten.LAENGENGRAD%type,
    v_BREITENGRAD in koordinaten.BREITENGRAD%type
)
is
begin
    intern__add_mietobjekt_form(
        v_titel,
        v_desc,
        PRODID.nextval,
        PRODID.nextval,
        v_mietpreis_HS,
        v_mietpreis_NS,
        v_status,
        v_vermieter_nr,
        PRODID.nextval,
        v_POSTLEITZAHL,
        v_STADT,
        v_STRASSE,
        v_HAUSNUMMER,
        PRODID.nextval,
        v_LAENGENGRAD,
        v_BREITENGRAD
    );
end;



begin
    add_mietobjekt_form(
        'Wohungn in Zentrum Nähe',
        'Wohnung in der Südvorstad',
      20,
        20,
        0,
        201348,
        '04275',
        'leipzig',
        'karli',
        15,
        40,
        30
    );
end;


