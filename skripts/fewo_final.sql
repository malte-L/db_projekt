-- DATENBANK

create table adresse
(
    adress_nr    int primary key,
    postleitzahl varchar2(5)  not null,
    stadt        varchar2(32) not null,
    strasse      varchar2(32) not null,
    hausnummer   varchar2(5)         not null
);

create table koordinaten
(
    koordinaten_nr int
        primary key,
    laengengrad    int not null,
    breitengrad    int not null
);

create table nutzer
(
    nutzer_nr      int
        primary key,
    name          varchar2(32)  not null,
    vorname       varchar2(32)  not null,
    geschlecht    int          null,
    e_mail      varchar2(128) not null,
    passwort_hash varchar2(32)  not null,
    adress_nr     int          null,
    constraint hat_adresse
        foreign key (adress_nr) references adresse (adress_nr)
);

create table mietarbeiter
(
    nutzer_nr int          not null
        primary key,
    typ       int      not null,
    firma     varchar2(128) null,
    constraint mietarbeiter_isa
        foreign key (nutzer_nr) references nutzer (nutzer_nr)
            on delete cascade
);

create table mieter
(
    nutzer_nr int not null
        primary key,
    constraint mieter_isa
        foreign key (nutzer_nr) references nutzer (nutzer_nr)
            on delete cascade
);

create table standort
(
    standort_nr    int
        primary key,
    koordinaten_nr int not null,
    adress_nr      int not null,
    constraint adress_nr
        foreign key (adress_nr) references adresse (adress_nr),
    constraint koordinaten_nr
        foreign key (koordinaten_nr) references koordinaten (koordinaten_nr)
            on delete cascade
);

create table vermieter
(
    nutzer_nr    int           not null
        primary key,
    beschreibung varchar2(2048) null,
    constraint vermieter_isa
        foreign key (nutzer_nr) references nutzer (nutzer_nr)
);

create table mietobjekt
(
    mietobjekt_nr int
        primary key,
    standort_nr int                  not null,
    mietpreis_hs  decimal(8, 2)        null,
    mietpreis_ns  decimal(8, 2)        null,
    bewertung     decimal(1)           null,
    status        decimal(1)             not null,
    vermieter_nr  int              not null,
    constraint hat_vermieter
        foreign key (vermieter_nr) references vermieter (nutzer_nr)
);

create table mangel
(
    kurze_beschreibung varchar2(128)  null,
    beschreibung       varchar2(2048) null,
    mietobjekt_nr      int       null,
    gemeldetvon        int       not null,
    constraint gemeldetvon_fk
        foreign key (gemeldetvon) references nutzer (nutzer_nr),
    constraint hat_mangel
        foreign key (mietobjekt_nr) references mietobjekt (mietobjekt_nr) on delete cascade
);

create table optionen
(
    mietobjekt_nr  int     not null,
    option_nr      int     not null primary key,
    kategorie      varchar2(32) null,
    schluesselwert int     not null,
    constraint hat_optionale_eigenschaft
        foreign key (mietobjekt_nr) references mietobjekt (mietobjekt_nr) on delete cascade
);

create table option_boolean
(
    option_nr int    not null,
    wert      int not null,
    constraint i_sa
        foreign key (option_nr) references optionen (option_nr)
);

create table option_integer
(
    option_nr int not null,
    wert      int not null,
    constraint is_a
        foreign key (option_nr) references optionen (option_nr)
);

create table option_string
(
    option_nr int           not null,
    wert      varchar2(2048) not null,
    constraint option_nr_fk
        foreign key (option_nr) references optionen (option_nr)
);

create table reszension
(
    inhalt        varchar2(2048) null,
    titel         varchar2(128)  null,
    bewertung     int       null,
    verfasst_am   date      not null,
    reszension_nr int           not null primary key,
    mietobjekt_nr int       not null,
    nutzer_nr     int       not null,
    constraint bewertet
        foreign key (mietobjekt_nr) references mietobjekt (mietobjekt_nr) on delete cascade,
    constraint verfasst_von
        foreign key (nutzer_nr) references mieter (nutzer_nr)
);

create table vermietung
(
    vermietung_nr int primary key,
    von           date not null,
    bis           date not null,
    mietobjekt_nr int      not null,
    nutzer_nr     int      not null,
    status        int      null,
    constraint mieter_ist
        foreign key (nutzer_nr) references mieter (nutzer_nr),
    constraint mietobjekt_nr
        foreign key (mietobjekt_nr) references mietobjekt (mietobjekt_nr)
);

create table zimmer
(
    zimmer_nr     int
        primary key,
    groesse       int null,
    mietobjekt_nr int null,
    constraint zimmerplan
        foreign key (mietobjekt_nr) references mietobjekt (mietobjekt_nr)
);


-- VIEWS

create or replace view mietobjekte as
select distinct
       MIETOBJEKT_NR, titel,beschreibung, anzahl_vermietungen, MIETPREIS_NS, MIETPREIS_HS, STATUS, NAME Vermieter_Name, VORNAME Vermieter_Vorname, VERMIETER_NR
       POSTLEITZAHL, STRASSE, HAUSNUMMER, STADT
from MIETOBJEKT mo
natural join STANDORT
natural join ADRESSE
left join (
    select Name,VORNAME, NUTZER_NR
    from nutzer
    natural join VERMIETER
    ) n
    on n.NUTZER_NR = mo.VERMIETER_NR
left join (
    select MIETOBJEKT_NR, count(MIETOBJEKT_NR) anzahl_vermietungen
    from VERMIETUNG
    natural join MIETOBJEKT
    group by MIETOBJEKT_NR
    )
    using(MIETOBJEKT_NR)
left join (
    select wert titel, MIETOBJEKT_NR
    from OPTIONEN
    natural join OPTION_STRING
    where KATEGORIE = 'meta'
        and SCHLUESSELWERT = 'titel'
    )
    using(MIETOBJEKT_NR)
left join (
    select wert beschreibung, MIETOBJEKT_NR
    from OPTIONEN
    natural join OPTION_STRING
    where KATEGORIE = 'meta'
        and SCHLUESSELWERT = 'desc'
    )
    using(MIETOBJEKT_NR);


create view TOP10_MIETOBJEKTE as
select "MIETOBJEKT_NR","GESAMT_BEWERTUNG", "ANZAHL_BEWETUNG"
from (
    select MIETOBJEKT_NR, ROUND(avg(RESZENSION.BEWERTUNG), 2) GESAMT_BEWERTUNG, COUNT(RESZENSION.BEWERTUNG) ANZAHL_BEWETUNG from
    MIETOBJEKT
    inner join RESZENSION using(MIETOBJEKT_NR)
    group by MIETOBJEKT_NR)
where
      ROWNUM <= 10
order by gesamt_bewertung desc



create or replace view MIETDAUER_TAGE as
SELECT floor((SUM(BIS-VON))/COUNT(*)) AS MIETDAUER_TAG FROM VERMIETUNG;



create view TOP5MIETDAUER as
select
       "MIETDAUER","NAME","VORNAME"
from (
    select floor(sum(v.bis - v.von)) Mietdauer, n.NAME, n.VORNAME
    from VERMIETUNG v, Nutzer n
    where
        v.NUTZER_NR = n.NUTZER_NR
        and v.STATUS in (1,2)
    group by v.NUTZER_NR, n.NAME, n.VORNAME
    order by Mietdauer desc
    )
where
    ROWNUM <= 5;


-- TRIGGER

CREATE OR REPLACE TRIGGER ENTFERNE_ADRESSE AFTER DELETE ON NUTZER FOR EACH ROW DELETE FROM ADRESSE WHERE ADRESS_NR = ADRESSE.ADRESS_NR;


--PROCEDUREN

-- wir hatten erst Probleme damit, Variablen in Proceduren zu deklarieren,
-- da das normale declare [vars] begin [...] end nicht bei Prozeduren funktioniert
-- später sind wir auf die Idee gekommen, einfach innerhalb des Code Blocks von 
-- der Prozedur noch einen Code Block zu erstellen (siehe: "maybe_vermiete"), dort kann mann dann "declare" verwenden.
-- Vorher haben wir als workaround zu jeder Prozedur eine intern__ Prozedur erstellt,
-- welche mehr parameter hat als die normale Prozedur. 
-- Diese extra Parameter sind die Variablen innerhalb der intern__ Prozedur
-- und die Werte dieser Variablen werden von der normalen Prozedur übergeben.


create or replace procedure maybe_vermiete(
    v_von VERMIETUNG.VON%type,
    v_bis VERMIETUNG.bis%type,
    v_nutzer_nr VERMIETUNG.nutzer_nr%type,
    v_mietobjekt_nr VERMIETUNG.MIETOBJEKT_NR%type
) is
begin
    declare
        dummy number;
        v_vermietung_nr VERMIETUNG.VERMIETUNG_NR%type := PRODID.nextval; -- PRODID.nextval als default hat nicht funktioniert
    begin
        select VERMIETUNG_NR into dummy from VERMIETUNG
        where MIETOBJEKT_NR = v_mietobjekt_nr
        -- and STATUS = 2 -- schon vom vermieter bestätigte vermietung
        and ((v_von between (VON-1) and (BIS+1)) or (v_bis between (VON-1) and (BIS+1)))
        and rownum = 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN begin
        insert into VERMIETUNG (VON, BIS, MIETOBJEKT_NR, NUTZER_NR, VERMIETUNG_NR)
        values (v_von, v_bis, v_mietobjekt_nr, v_nutzer_nr, v_vermietung_nr);
        commit;
        return;
      end;
    end;

    raise_application_error(-20111, 'Mietzeitraum schon vergeben');
end;


--  das ist z.B. so eine Prozedur, wo wir den workaround verwendet haben
create or replace procedure intern__add_mietobjekt_form (
    v_titel in varchar2,
    v_desc in varchar2,
    v_standort_nr in standort.standort_nr%type,
    v_mietobjekt_nr in mietobjekt.mietobjekt_nr%type,
    v_mietpreis_HS in mietobjekt.mietpreis_HS%type,
    v_mietpreis_NS in mietobjekt.mietpreis_NS%type,
    v_status in mietobjekt.status%type,
    v_vermieter_nr in mietobjekt.vermieter_nr%type,
    v_ADRESS_NR in adresse.ADRESS_NR%type,
    v_POSTLEITZAHL in adresse.POSTLEITZAHL%type,
    v_STADT in adresse.STADT%type,
    v_STRASSE in adresse.STRASSE%type,
    v_HAUSNUMMER in adresse.HAUSNUMMER%type,
    v_KOORDINATEN_NR in koordinaten.KOORDINATEN_NR%type,
    v_LAENGENGRAD in koordinaten.LAENGENGRAD%type,
    v_BREITENGRAD in koordinaten.BREITENGRAD%type
)
is
begin
    insert into ADRESSE (ADRESS_NR, POSTLEITZAHL, STADT, STRASSE, HAUSNUMMER)
    values (v_ADRESS_NR, v_POSTLEITZAHL, v_STADT, v_STRASSE, v_HAUSNUMMER);

    insert into KOORDINATEN (KOORDINATEN_NR, LAENGENGRAD, BREITENGRAD)
    values (v_KOORDINATEN_NR, v_LAENGENGRAD, v_BREITENGRAD);

    insert into STANDORT (STANDORT_NR, KOORDINATEN_NR, ADRESS_NR)
    values (v_standort_nr, v_KOORDINATEN_NR, v_adress_nr);

    insert into MIETOBJEKT (MIETOBJEKT_NR, STANDORT_NR, MIETPREIS_HS, MIETPREIS_NS, STATUS, VERMIETER_NR)
    values (v_mietobjekt_nr, v_standort_nr, v_mietpreis_HS, v_mietpreis_NS, v_status, v_vermieter_nr);

    ADD_STRING_OPTION(v_mietobjekt_nr, 'meta', 'titel', v_titel);
    ADD_STRING_OPTION(v_mietobjekt_nr, 'meta', 'desc', v_desc);
    commit;
end;


create or replace procedure add_mietobjekt_form (
    v_titel in varchar2,
    v_desc in varchar2,
    v_mietpreis_HS in mietobjekt.mietpreis_HS%type,
    v_mietpreis_NS in mietobjekt.mietpreis_NS%type,
    v_status in mietobjekt.status%type,
    v_vermieter_nr in mietobjekt.vermieter_nr%type,
    v_POSTLEITZAHL in adresse.POSTLEITZAHL%type,
    v_STADT in adresse.STADT%type,
    v_STRASSE in adresse.STRASSE%type,
    v_HAUSNUMMER in adresse.HAUSNUMMER%type,
    v_LAENGENGRAD in koordinaten.LAENGENGRAD%type,
    v_BREITENGRAD in koordinaten.BREITENGRAD%type
)
is
begin
    intern__add_mietobjekt_form(
        v_titel,
        v_desc,
        PRODID.nextval,
        PRODID.nextval,
        v_mietpreis_HS,
        v_mietpreis_NS,
        v_status,
        v_vermieter_nr,
        PRODID.nextval,
        v_POSTLEITZAHL,
        v_STADT,
        v_STRASSE,
        v_HAUSNUMMER,
        PRODID.nextval,
        v_LAENGENGRAD,
        v_BREITENGRAD
    );
end;
--





create or replace procedure add_boolean_option(
    v_mietobjekt_nr in MIETOBJEKT.MIETOBJEKT_NR%type,
    v_kategorie in OPTIONEN.KATEGORIE%type,
    v_schluessel_wert in OPTIONEN.SCHLUESSELWERT%type,
    v_wert in OPTION_BOOLEAN.WERT%type
)  is
begin
    declare
         v_option_nr OPTIONEN.OPTION_NR%type := PRODID.nextval;
    begin
        insert into OPTIONEN (MIETOBJEKT_NR, OPTION_NR, KATEGORIE, SCHLUESSELWERT)
        values (v_mietobjekt_nr, v_option_nr, v_kategorie, v_schluessel_wert);

        insert into OPTION_BOOLEAN (OPTION_NR, WERT)
        values (v_option_nr, v_wert);
    end;
end;


create or replace procedure add_string_option(
    v_mietobjekt_nr in MIETOBJEKT.MIETOBJEKT_NR%type,
    v_kategorie in OPTIONEN.KATEGORIE%type,
    v_schluessel_wert in OPTIONEN.SCHLUESSELWERT%type,
    v_wert in option_string.WERT%type
)  is
begin
    declare
        v_option_nr OPTIONEN.OPTION_NR%type := PRODID.nextval;
    begin
        insert into OPTIONEN (MIETOBJEKT_NR, OPTION_NR, KATEGORIE, SCHLUESSELWERT)
        values (v_mietobjekt_nr, v_option_nr, v_kategorie, v_schluessel_wert);

        insert into option_string (OPTION_NR, WERT)
        values (v_option_nr, v_wert);
    end;
end;

create or replace procedure add_integer_option(
    v_mietobjekt_nr in MIETOBJEKT.MIETOBJEKT_NR%type,
    v_kategorie in OPTIONEN.KATEGORIE%type,
    v_schluessel_wert in OPTIONEN.SCHLUESSELWERT%type,
    v_wert in option_integer.WERT%type
)  is
begin
    declare
        v_option_nr  OPTIONEN.OPTION_NR%type := PRODID.nextval;
    begin
        insert into OPTIONEN (MIETOBJEKT_NR, OPTION_NR, KATEGORIE, SCHLUESSELWERT)
        values (v_mietobjekt_nr, v_option_nr, v_kategorie, v_schluessel_wert);

        insert into option_integer (OPTION_NR, WERT)
        values (v_option_nr, v_wert);
    end;
end;



