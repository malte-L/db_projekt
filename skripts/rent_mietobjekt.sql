create or replace procedure internal__rent_mietobjekt(
    v_VON in vermietung.VON%type,
    v_BIS in vermietung.BIS%type,
    v_MIETOBJEKT_NR in vermietung.MIETOBJEKT_NR%type,
    v_NUTZER_NR in vermietung.NUTZER_NR%type,
    v_VERMIETUNG_NR in vermietung.VERMIETUNG_NR%type
) is
begin
    insert into VERMIETUNG(VERMIETUNG_NR, VON, BIS, MIETOBJEKT_NR, NUTZER_NR)
    values (v_VERMIETUNG_NR, v_VON, v_BIS, v_MIETOBJEKT_NR, v_NUTZER_NR);
    commit;
end;


create or replace procedure rent_mietobjekt(
    v_VON in vermietung.VON%type,
    v_BIS in vermietung.BIS%type,
    v_MIETOBJEKT_NR in vermietung.MIETOBJEKT_NR%type,
    v_NUTZER_NR in vermietung.NUTZER_NR%type
) is
begin
    internal__rent_mietobjekt(
        v_VON,
        v_BIS,
        v_MIETOBJEKT_NR,
        v_NUTZER_NR,
        PRODID.nextval
    );
    commit;
end;



begin
    rent_mietobjekt(
        trunc(sysdate),
        trunc(sysdate),
        200521,
        200518
    );
end;