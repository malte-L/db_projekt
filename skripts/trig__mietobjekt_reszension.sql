create or replace trigger after_insert_reszension
after insert on RESZENSION
    for each row
declare
    new_bewertung mietobjekt.bewertung%type;
    sum_bewertung reszension.bewertung%type;
    anzahl number;
begin
    select sum(BEWERTUNG), count(*) into sum_bewertung, anzahl
    from RESZENSION
    where MIETOBJEKT_NR = :new.MIETOBJEKT_NR;

    new_bewertung := sum_bewertung / anzahl;

    update MIETOBJEKT set BEWERTUNG = 1
    where MIETOBJEKT_NR = :new.MIETOBJEKT_NR;
end;
