create or replace view mietobjekte as
select distinct
       MIETOBJEKT_NR, titel,beschreibung, anzahl_vermietungen, MIETPREIS_NS, MIETPREIS_HS, STATUS, NAME Vermieter_Name, VORNAME Vermieter_Vorname, VERMIETER_NR
       POSTLEITZAHL, STRASSE, HAUSNUMMER, STADT
from MIETOBJEKT mo
natural join STANDORT
natural join ADRESSE
left join (
    select Name,VORNAME, NUTZER_NR
    from nutzer
    natural join VERMIETER
    ) n
    on n.NUTZER_NR = mo.VERMIETER_NR
left join (
    select MIETOBJEKT_NR, count(MIETOBJEKT_NR) anzahl_vermietungen
    from VERMIETUNG
    natural join MIETOBJEKT
    group by MIETOBJEKT_NR
    )
    using(MIETOBJEKT_NR)
left join (
    select wert titel, MIETOBJEKT_NR
    from OPTIONEN
    natural join OPTION_STRING
    where KATEGORIE = 'meta'
        and SCHLUESSELWERT = 'titel'
    )
    using(MIETOBJEKT_NR)
left join (
    select wert beschreibung, MIETOBJEKT_NR
    from OPTIONEN
    natural join OPTION_STRING
    where KATEGORIE = 'meta'
        and SCHLUESSELWERT = 'desc'
    )
    using(MIETOBJEKT_NR);


create view TOP10_MIETOBJEKTE as
select "MIETOBJEKT_NR","GESAMT_BEWERTUNG", "ANZAHL_BEWETUNG"
from (
    select MIETOBJEKT_NR, ROUND(avg(RESZENSION.BEWERTUNG), 2) GESAMT_BEWERTUNG, COUNT(RESZENSION.BEWERTUNG) ANZAHL_BEWETUNG from
    MIETOBJEKT
    inner join RESZENSION using(MIETOBJEKT_NR)
    group by MIETOBJEKT_NR)
where
      ROWNUM <= 10
order by gesamt_bewertung desc


create or replace view MIETDAUER_TAGE as
SELECT floor((SUM(BIS-VON))/COUNT(*)) AS MIETDAUER_TAG FROM VERMIETUNG;



create view TOP5MIETDAUER as
select
       "MIETDAUER","NAME","VORNAME"
from (
    select floor(sum(v.bis - v.von)) Mietdauer, n.NAME, n.VORNAME
    from VERMIETUNG v, Nutzer n
    where
        v.NUTZER_NR = n.NUTZER_NR
        and v.STATUS in (1,2)
    group by v.NUTZER_NR, n.NAME, n.VORNAME
    order by Mietdauer desc
    )
where
    ROWNUM <= 5;


declare
    adress_nr_1 number;
    mieter_nr_0 number;
    mietobj_nr_0 number;
    verm_nr_0 number;
    adress_nr_0 number;

    stadt varchar2(20);
begin
    adress_nr_1 := PRODID.nextval;
    mieter_nr_0 := PRODID.nextval;
    mietobj_nr_0 := PRODID.nextval;
    verm_nr_0 := PRODID.nextval;
    adress_nr_0 := PRODID.nextval;

    stadt := 'Frankfurt';

    INTERN__ADD_VERMIETER_FORM(verm_nr_0, adress_nr_0,'Westmann','Ossie',0,'ossie@gmx.de','wad238909=K"??','.','04103',stadt,'zum bahnhof','420');
    INTERN__ADD_MIETER_FORM(mieter_nr_0,adress_nr_1,'Furt','Frank',1,'frank@furt.xyz','#n/IKZ>].}kIM]^/no','187',stadt,'am bahnhof','69');
    intern__add_mietobjekt_form(PRODID.nextval,mietobj_nr_0,1,0.5,0,verm_nr_0,PRODID.nextval,'187',stadt,'eisenbahnstrasse','17b',PRODID.nextval,15,15);

    insert into VERMIETUNG (VON, BIS, MIETOBJEKT_NR, NUTZER_NR, STATUS, VERMIETUNG_NR)
    values (sysdate - floor(PRODID.nextval / 1000), sysdate - floor(PRODID.nextval / 1000) + 4, mietobj_nr_0, mieter_nr_0, 0, PRODID.nextval);

    insert into VERMIETUNG (VON, BIS, MIETOBJEKT_NR, NUTZER_NR, STATUS, VERMIETUNG_NR)
    values (sysdate - floor(PRODID.nextval / 1000),  sysdate -floor(PRODID.nextval / 1000) + 5, mietobj_nr_0, mieter_nr_0, 0, PRODID.nextval);
end;


